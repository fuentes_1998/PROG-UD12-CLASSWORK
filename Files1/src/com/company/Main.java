package com.company;

import java.io.*;

public class Main {

    public static void main(String[] args) {

        String directory = "resources";
        String fileName = "fitxer1.txt";

        createFile(directory, fileName);
        seeContent(directory);
        seeInfo(directory,fileName);
    }

    private static void createFile(String directory, String fileName) {

        File file = new File(directory, fileName);

        try {

            if (file.createNewFile()) {

                System.out.println("El fichero ha sido creado");
            }

        } catch(IOException e) {

            e.printStackTrace();
        }
    }

    private static void seeContent(String directory) {

        File file = new File(directory);

        try {

            FileReader fileReader = new FileReader(file.getName());

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String linea;

            while((linea=bufferedReader.readLine())!=null) {

                System.out.println(linea);
            }

            bufferedReader.close();
            fileReader.close();

        } catch(IOException e) {

            e.printStackTrace();
        }
    }

    private static void seeInfo(String directory, String fileName) {

        File file = new File(directory, fileName);

        System.out.println("Name: " + file.getName());

        System.out.println("Absuolute Path: " + file.getAbsolutePath());
        System.out.println("Relative Path: "+file.getPath());
        System.out.println("Can Write: "+file.canWrite());
        System.out.println("Can Read: "+file.canRead());
        System.out.println("Length: "+file.length()/8/1024f+"Mb");

        if (file.isDirectory()) {

            System.out.println("Directory");

        } else {

            System.out.println("File");

        }
    }
}
