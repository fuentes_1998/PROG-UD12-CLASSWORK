package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        String fileName = "fitxer2.txt";
        String directory = "resources";

        File file = new File(directory + File.separator + fileName);

        read(file);
    }

    private static void read(File file){

        try {
            String cadena;

            FileReader fileReader = new FileReader(file);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((cadena = bufferedReader.readLine()) != null) {

                System.out.println(cadena);
            }


        }  catch (IOException e) {

            e.printStackTrace();

        }
    }
}
